# symfony/web-server-bundle

Running applications using the PHP built-in web server https://packagist.org/packages/symfony/web-server-bundle

[![PHPPackages Rank](http://phppackages.org/p/symfony/web-server-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/web-server-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/web-server-bundle/badge/referenced-by.svg)](http://phppackages.org/p/symfony/web-server-bundle)
* [referenced-by](https://phppackages.org/p/symfony/web-server-bundle/referenced-by)

## [Recipe](https://github.com/symfony/recipes/tree/master/symfony/web-server-bundle)

## Official documentation
* [*How to Use PHP's built-in Web Server*](https://symfony.com/web-server-bundle)
* [*Symfony Local Web Server*](https://symfony.com/doc/current/setup/symfony_server.html)
  (replacement from symfony/cli).

### Deprecated...
* "deprecated WebserverBundle" in [*A Week of Symfony #654 (8-14 July 2019)*
  ](https://symfony.com/blog/a-week-of-symfony-654-8-14-july-2019)
  2019-07 Javier Eguiluz
